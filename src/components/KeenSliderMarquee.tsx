import { useKeenSlider } from "keen-slider/react";
import "keen-slider/keen-slider.min.css";

const animation = { duration: 20000, easing: (t: number) => t };

export default function KeenSliderMarquee() {
  const [sliderRef] = useKeenSlider<HTMLDivElement>({
    loop: true,
    renderMode: "performance",
    slides: { perView: 1 },
    breakpoints: {
      "(min-width: 768px)": {
        slides: { perView: 2 },
      },
      "(min-width: 1280px)": {
        slides: { perView: 3 },
      },
    },
    created(s) {
      s.moveToIdx(3, true, animation);
    },
    updated(s) {
      s.moveToIdx(s.track.details.abs + 3, true, animation);
    },
    animationEnded(s) {
      s.moveToIdx(s.track.details.abs + 2, true, animation);
    },
  });
  return (
    <div
      ref={sliderRef}
      className="keen-slider mx-auto max-w-7xl border-4 border-black"
    >
      <div className="keen-slider__slide">
        <div
          className="h-[43.75rem]"
          onClick={() => console.log("box 1 clicked")}
        >
          <img
            src="https://images.unsplash.com/photo-1718879329566-82d40c4175d6?q=80&w=1976&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
      </div>
      <div className="keen-slider__slide">
        <div
          className="h-[21.875rem]"
          onClick={() => console.log("box 2 clicked")}
        >
          <img
            src="https://images.unsplash.com/photo-1718675529384-3137fb480449?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
        <div
          className="h-[21.875rem]"
          onClick={() => console.log("box 3 clicked")}
        >
          <img
            src="https://images.unsplash.com/photo-1719420062178-8675f842252a?q=80&w=2111&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
      </div>
      <div className="keen-slider__slide">
        <div
          className="h-[43.75rem]"
          onClick={() => console.log("box 4 clicked")}
        >
          <img
            src="https://images.unsplash.com/photo-1719230693591-481437cbd4a5?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
      </div>
      <div className="keen-slider__slide">
        <div
          className="h-[21.875rem]"
          onClick={() => console.log("box 5 clicked")}
        >
          <img
            src="https://images.unsplash.com/photo-1719437492375-1c20c8c17846?q=80&w=2003&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
        <div
          className="h-[21.875rem]"
          onClick={() => console.log("box 6 clicked")}
        >
          <img
            src="https://images.unsplash.com/photo-1719304247081-6647a664bc65?q=80&w=1975&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
      </div>
    </div>
  );
}
