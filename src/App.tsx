import SwiperMarquee from "./components/SwiperMarquee";
import KeenSliderMarquee from "./components/KeenSliderMarquee";

export default () => {
  return (
    <div className="py-5">
      <h2 className="p-5 pt-0 text-center text-3xl font-bold">Swiper</h2>
      <SwiperMarquee />
      <h2 className="p-5 pt-5 text-center text-3xl font-bold">Keen Slider</h2>
      <KeenSliderMarquee />
    </div>
  );
};
